Android app integrating/displaying Chemisense readings in realtime and outputting to HMI, calculates AQI values

ChemiSense sensor optimized for old sensor version:

Channels:
- 1- "NO2"
- 2 - "NH3"
- 3 - "CO"
- 4 - "FCOH"
- 5 - "Temp"
- 6 - "HUM"
- 7 - "VOCs"
- 8 - "PM2.5"

Shows the external AQI (pulled from AirNow API) compared to cabin AQI (calculated from Chemisense readings by their algorithm)
